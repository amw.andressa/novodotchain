using System.Diagnostics.CodeAnalysis;
using System.Text.Json;

namespace DotChain.Extensions;

[ExcludeFromCodeCoverage]
public static class JsonExtensions
{
    public static string ToJson(this object data)
    {
        return JsonSerializer.Serialize(data, new JsonSerializerOptions()
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        });        
    }
    
    public static T? FromJson<T>(this string json)
    {
        return JsonSerializer.Deserialize<T>(json, new JsonSerializerOptions()
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        });        
    }    
}